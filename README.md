# Suitarr

Suitarr is a collection of docker images. By using different docker tags, you'll install different apps. Suitarr also gives you the option of installing different versions for every app.

## Donations

NANO: `xrb_1bxqm6nsm55s64rgf8f5k9m795hda535to6y15ik496goatakpupjfqzokfc`  
BTC: `39W6dcaG3uuF5mZTRL4h6Ghem74kUBHrmz`  
LTC: `MMUFcGLiK6DnnHGFnN2MJLyTfANXw57bDY`

## Starting the container

The environment variables `PUID`, `PGID`, `UMASK`, `VERSION` and `BACKUP` are all optional, the values you see below are the default values.

By default the image comes with a stable version. You can however install a different version with the environment variable `VERSION`. The value `image` does nothing, but keep the included version, all the others install a different version when starting the container.

### [Radarr](https://github.com/Radarr/Radarr)

```shell
docker run --rm \
           --name radarr \
           -p 7878:7878 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:radarr
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/Radarr/Radarr/releases/download/v0.2.0.1120/Radarr.develop.0.2.0.1120.linux.tar.gz
-e VERSION=file:///config/Radarr.develop.0.2.0.1120.linux.tar.gz
```

### [Sonarr](https://github.com/Sonarr/Sonarr)

```shell
docker run --rm \
           --name sonarr \
           -p 8989:8989 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:sonarr
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=v3
-e VERSION=http://download.sonarr.tv/v2/master/mono/NzbDrone.master.2.0.0.5228.mono.tar.gz
-e VERSION=file:///config/NzbDrone.master.2.0.0.5228.mono.tar.gz
```

### [Lidarr](https://github.com/lidarr/Lidarr)

```shell
docker run --rm \
           --name lidarr \
           -p 8686:8686 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:lidarr
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/lidarr/Lidarr/releases/download/v0.3.0.430/Lidarr.develop.0.3.0.430.linux.tar.gz
-e VERSION=file:///config/Lidarr.develop.0.3.0.430.linux.tar.gz
```

### [Jackett](https://github.com/Jackett/Jackett)

```shell
docker run --rm \
           --name jackett \
           -p 9117:9117 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:jackett
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/Jackett/Jackett/releases/download/v0.8.1162/Jackett.Binaries.Mono.tar.gz
-e VERSION=file:///config/Jackett.Binaries.Mono.tar.gz
```

### [NZBHydra2](https://github.com/theotherp/nzbhydra2)

```shell
docker run --rm \
           --name nzbhydra2 \
           -p 5076:5076 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:nzbhydra2
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/theotherp/nzbhydra2/releases/download/v1.5.1/nzbhydra2-1.5.1-linux.zip
-e VERSION=file:///config/nzbhydra2-1.5.1-linux.zip
```

### [NZBGet](https://github.com/nzbget/nzbget)

```shell
docker run --rm \
           --name nzbget \
           -p 6789:6789 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:nzbget
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/nzbget/nzbget/releases/download/v20.0/nzbget-20.0-bin-linux.run
-e VERSION=file:///config/nzbget-20.0-bin-linux.run
```

### [Bazarr](https://github.com/morpheus65535/bazarr)

```shell
docker run --rm \
           --name bazarr \
           -p 6767:6767 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:bazarr
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/morpheus65535/bazarr/archive/master.tar.gz
-e VERSION=file:///config/master.tar.gz
```

### [SABnzbd](https://github.com/sabnzbd/sabnzbd)

```shell
docker run --rm \
           --name sabnzbd \
           -p 8080:8080 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:sabnzbd
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/sabnzbd/sabnzbd/releases/download/2.3.4/SABnzbd-2.3.4-src.tar.gz
-e VERSION=file:///config/SABnzbd-2.3.4-src.tar.gz
```

### [Ombi](https://github.com/tidusjar/Ombi)

```shell
docker run --rm \
           --name ombi \
           -p 5000:5000 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:ombi
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/tidusjar/Ombi/releases/download/v3.0.3477/linux.tar.gz
-e VERSION=file:///config/linux.tar.gz
```

### [Tautulli](https://github.com/Tautulli/Tautulli)

```shell
docker run --rm \
           --name tautulli \
           -p 8181:8181 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:tautulli
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/Tautulli/Tautulli/archive/v2.1.14.tar.gz
-e VERSION=file:///config/v2.1.14.tar.gz
```

### [Nanode](https://github.com/nanocurrency/raiblocks)

```shell
docker run --rm \
           --name nanode \
           -p 7075:7075/udp \
           -p 7075:7075 \
           -p 127.0.0.1:7076:7076 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:nanode
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/nanocurrency/raiblocks/releases/download/V16.0/nano-16.0.0-Linux.tar.bz2
-e VERSION=file:///config/nano-16.0.0-Linux.tar.bz2
```

### [Nanomon](https://github.com/NanoTools/nanoNodeMonitor)

```shell
docker run --rm \
           --name nanomon \
           -p 8081:8081 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           hotio/suitarr:nanomon
```

```shell
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/NanoTools/nanoNodeMonitor/archive/v1.4.10.tar.gz
-e VERSION=file:///config/v1.4.10.tar.gz
```

## GitLab Registry

The GitLab registry is used for testing, all commits are build and pushed to the GitLab registry. When everything seems to be working, the images get pushed to the DockerHub registry. You can use the GitLab registry, by changing `hotio/suitarr:...` to `registry.gitlab.com/hotio/suitarr:...`.

## Backing up the configuration

By default on every docker container shutdown a backup is created from the configuration files. To disable this, use `-e BACKUP=no`.

## Additional app arguments

You can use the following environment variable to pass on additional arguments to your app.

```shell
-e ARGS="--ProxyConnection localhost:8118"
```

## Using a rclone mount

Mounting a remote filesystem using `rclone` can be done with the option `-e RCLONE="..."`, see below for a more detailed example. Use `docker exec -it --user hotio CONTAINERNAME rclone config` to configure your remote when the container is running. Configuration files for `rclone` are stored in `/config/.config/rclone`.

```shell
-e RCLONE="remote1:path/to/files,/localmount1|remote2:path/to/files,/localmount2"
```

In most cases you will need some or all of the following flags added to your command to get the required docker privileges.

```shell
 --security-opt apparmor:unconfined --cap-add SYS_ADMIN --device /dev/fuse
```

## Using a rar2fs mount

Mounting a filesystem using `rar2fs` can be done with the option `-e RAR2FS="..."`, see below for a more detailed example. The new mount will be read-only. Using a `rar2fs` mount makes the use of an unrar script obsolete. You can mount a `rar2fs` mount on top of an `rclone` mount, `rclone` mounts are mounted first.

```shell
-e RAR2FS="/folder1-rar,/folder1-unrar|/folder2-rar,/folder2-unrar"
```

In most cases you will need some or all of the following flags added to your command to get the required docker privileges.

```shell
 --security-opt apparmor:unconfined --cap-add SYS_ADMIN --device /dev/fuse
```

## Quick one-liners to test a container

```shell
sudo docker run --rm --name testcontainer -p 9999:7878 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:radarr
sudo docker run --rm --name testcontainer -p 9999:8989 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:sonarr
sudo docker run --rm --name testcontainer -p 9999:8686 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:lidarr
sudo docker run --rm --name testcontainer -p 9999:9117 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:jackett
sudo docker run --rm --name testcontainer -p 9999:5076 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:nzbhydra2
sudo docker run --rm --name testcontainer -p 9999:6789 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:nzbget
sudo docker run --rm --name testcontainer -p 9999:6767 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:bazarr
sudo docker run --rm --name testcontainer -p 9999:8080 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:sabnzbd
sudo docker run --rm --name testcontainer -p 9999:5000 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:ombi
sudo docker run --rm --name testcontainer -p 9999:8181 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:tautulli
sudo docker run --rm --name testcontainer -p 9999:8081 -v /etc/localtime:/etc/localtime:ro -v /tmp/testcontainer:/config -e VERSION=image registry.gitlab.com/hotio/suitarr:nanomon
```
