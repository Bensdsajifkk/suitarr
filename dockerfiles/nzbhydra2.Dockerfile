ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:base

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="nzbhydra2"
EXPOSE 5076
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:5076 || exit 1

# install packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
        openjdk-8-jre-headless && \
# clean up
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
# https://github.com/theotherp/nzbhydra2/releases
RUN url="https://github.com/theotherp/nzbhydra2/releases/download/v2.0.15/nzbhydra2-2.0.15-linux.zip" && \
    zipfile="/tmp/app.zip" && \
    curl -fsSL -o "${zipfile}" "${url}" && unzip -q "${zipfile}" -d "${APP_DIR}" && rm "${zipfile}" && \
    chmod +x "${APP_DIR}/nzbhydra2"

COPY root/ /
