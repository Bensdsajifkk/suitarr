ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:base

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="bazarr"
EXPOSE 6767
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:6767 || exit 1

# install app
# https://github.com/morpheus65535/bazarr/releases
RUN url="https://github.com/morpheus65535/bazarr/archive/V0.6.7.1.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}" --strip-components=1

COPY root/ /
