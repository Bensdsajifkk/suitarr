ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:base

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="nanode"
EXPOSE 7075/tcp 7075/udp 7076/tcp

# install app
# https://github.com/nanocurrency/raiblocks/releases
RUN url="https://github.com/nanocurrency/raiblocks/releases/download/V16.2/nano-16.2.0-Linux.tar.bz2" && \
    curl -fsSL "${url}" | tar xjf - -C "${APP_DIR}" --strip-components=1

COPY root/ /
