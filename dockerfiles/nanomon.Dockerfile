ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:base

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="nanomon"
EXPOSE 8081
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8081 || exit 1

# install packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
        apache2 php libapache2-mod-php php-curl && \
# clean up
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
# https://github.com/NanoTools/nanoNodeMonitor/releases
RUN url="https://github.com/NanoTools/nanoNodeMonitor/archive/v1.4.11.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}" --strip-components=1

COPY root/ /
