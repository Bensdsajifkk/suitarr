ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:mono

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="lidarr"
EXPOSE 8686
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8686 || exit 1

# install app
# https://github.com/lidarr/Lidarr/releases
RUN url="https://github.com/lidarr/Lidarr/releases/download/v0.4.0.524/Lidarr.develop.0.4.0.524.linux.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}" --strip-components=1

COPY root/ /
